/** @module styles/global */
import styled, { css, createGlobalStyle } from 'styled-components';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

/** The screen breakpoints configuration */
const screenSizes = {
  /* desktop large */
  dl: 1800,
  /* desktop */
  dt: 1200,
  /* tablet landscape */
  tl: 900,
  /* tablet portrait */
  tp: 600
};

/** The corresponding css code for screen breakpoints  */
export const media = Object.keys(screenSizes).reduce((acc, label) => {
  acc[label] = (...args) => css`
    @media (min-width: ${screenSizes[label]}px) {
      ${css(...args)}
    }
  `;
  return acc;
}, {});

/* Tablet media queries */
const size = {
  tablet: '768px',
  tabletL: '1024px',
  tabletXL: '1366px'
};

export const device = {
  tabletP: `(min-width: ${size.tablet}) and (max-device-width : ${size.tabletL}) and (orientation : portrait)`,
  tabletL: `(min-width: ${size.tablet}) and (max-device-width : ${size.tabletL}) and (orientation : landscape)`,
  tabletXL: `(min-width: ${size.tabletL}) and (max-device-width : ${size.tabletXL})`,
  tabletPRT: `(min-width: ${size.tabletL}) and (max-device-width : ${size.tablet})`
};

/**
 * Internet Explorer 11 media query template
 * @function
 * @param {array} args - arguments to pass to css method
 * @return {string} css template from styled API
 */
export const ie11 = (...args) => css`
  @media all and (-ms-high-contrast: none), (-ms-high-contrast: active) {
    ${css(...args)}
  }
`;

const GlobalStyle = createGlobalStyle`
  body {
    background-color: ${({ theme }) => theme.grey100};
  }
`;

export const FlexRow = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  text-align: left;
  white-space: normal;
  word-break: break-word;
  overflow: hidden;
`;

export const FlexTop = styled(FlexRow)`
  align-items: flex-start;
  vertical-align: top;
`;
export const FlexBottom = styled(FlexRow)`
  align-items: flex-end;
  vertical-align: bottom;
`;
export const FlexLeft = styled(FlexRow)`
  justify-content: flex-start;
  text-align: left;
`;
export const FlexRight = styled(FlexRow)`
  justify-content: flex-end;
  text-align: right;
`;
export const FlexCenter = styled(FlexRow)`
  justify-items: center;
  text-align: center;
`;

export const Flex1 = styled.div`
  flex: 1;
`;
export const Flex2 = styled.div`
  flex: 2;
`;
export const Flex3 = styled.div`
  flex: 3;
`;
export const Flex4 = styled.div`
  flex: 4;
`;

export default GlobalStyle;

/** The theme configuration */
export const theme = {
  primary: '#002469',
  secondary: '#E1091D',
  tertiary: '#002265',
  brand: '#00377B',

  success: '#5ABE75',
  success100: '#358542',
  warning: '#FFB321',
  error: '#E11849',

  blue100: '#2372D5',
  blue200: '#1A56A0',
  blue300: '#1F6EDA',
  blue400: '#4B48C6',

  teal50: '#D8F8EE',
  teal100: '#B2DFDB',
  teal200: '#72C1A9',
  teal300: '#00695C',
  teal400: '#36C7D0',

  cyan50: '#D8F5F8',
  cyan100: '#B2EBF2',
  cyan200: '#00BCD4',
  cyan300: '#00838F',
  cyan400: '#00818D',

  sky50: '#DDEFFD',
  sky100: '#BBDEFB',
  sky200: '#2196F3',
  sky300: '#104C90',

  marine50: '#E3E9FF',
  marine100: '#C7D3FF',
  marine200: '#2742C4',
  marine300: '#011371',

  pink50: '#F9DBE5',
  pink100: '#F8BBD0',
  pink200: '#F06292',
  pink300: '#AD1457',
  pink400: '#FF80A2',

  grey100: '#EAECED',
  grey200: '#DEE3EA',
  grey300: '#BEC8D4',
  grey400: '#96A0AB',
  grey500: '#8B8B8E',
  grey600: '#464D53',
  grey700: '#1E2227',
  grey800: '#7B7B7B',
  grey900: '#858587',
  grey1000: '#3A3A3C',
  greyA1: '#091E42',
  greyA2: '#42526E',
  greyA3: '#6B778C',

  orange50: '#FEF1D7',
  orange100: '#FFA48E',

  black: '#000000',
  white: '#FFFFFF',
  transparent: 'transparent',
  red: '#DB393D',

  statusInfo: '#2372D5',
  statusError: '#E11849',
  statusWarning: '#FF5722',
  statusSuccess: '#358542',
  positive: '#1B865E',

  green100: '#4ACFAC',
  purple100: '#7E8CE0'
};

export const partiesColors = ['teal300', 'cyan400', 'sky300', 'marine300'];
