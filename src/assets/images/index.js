export { default as financeHome } from './finance.jpeg';
export { default as crpHome } from './rp_bg.png';
export { default as retirement } from './retirement.png';
export { default as education } from './education.png';
export { default as selectGoals } from './select_goals.png';
export { default as goalCardDefaultBg } from './goalCard.png';
export { default as blue } from './blue.png';
export { default as explore } from './bg.png';
export { default as whitelogo } from './UOB_white.png';
export { default as cardDefaultBg } from './card-bg.png';
export { default as educationalGoal } from './educational_goal.png';
export { default as retirementGoal } from './retirement-goal.png';
export { default as waGoal } from './waGoal.png';
export { default as goalSummary } from './goal_summary.png';
export { default as enhanceAssets } from './enhance-asset.png';
export { default as medical } from './medical.png';
export { default as legacyProtection } from './legacyProtection.png';
export { default as mortgage } from './mortgage.png';
export { default as income } from './income.png';
export { default as contactRm } from './contact-rm.png';
export { default as ckaBanner } from './cka.png';
export { default as mobile } from './mobile.png';
