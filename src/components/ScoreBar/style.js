import styled from 'styled-components';
import { theme, FlexRow, FlexLeft, Flex1 } from 'global/index';

export const Container = styled.div`
  overflow: visible;
  color: #fff;
  margin-bottom: 6.4rem;
`;

export const ScoresWrapper = styled.div`
  padding: 3.2rem;
`;
export const Score = styled(Flex1)`
  color: ${theme.grey300};
  font-size: 1.2rem;
`;

export const ScoreDash = styled(Flex1)`
  height: 0.1rem;
  border-top: 0.1rem dashed ${theme.grey300};
`;
export const ScoreLabel = styled.div`
  height: 4.8rem;
  width: 4.8rem;
  line-height: 4.8rem;
  border: 0.1rem solid ${theme.grey200};
  background: ${({ color, active }) => (active ? color : theme.grey100)};
  ${({ active }) => (active ? `color: ${theme.grey600};` : '')}
  border-radius: 50%;
  text-align: center;
  text-transform: uppercase;
`;

export const Block = styled(FlexRow)`
  flex: 1;
`;

export const Bar = styled(Flex1)`
  height: 1.6rem;
  background: ${({ color }) => color};
  text-align: center;
  text-transform: uppercase;
`;

export const TitleWrapper = styled(FlexLeft, FlexRow)``;

export const Title = styled.div`
  margin-top: 0.8rem;
  color: ${theme.grey700};
  font-size: 1.6rem;
  line-height: 2.4rem;
  text-align: left;
  justify-content: flex-start;
`;

export { FlexRow };
