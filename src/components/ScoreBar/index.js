import React, { useEffect, useState, useCallback } from 'react';
import PropTypes from 'prop-types';

import {
  Container,
  ScoresWrapper,
  Score,
  ScoreDash,
  ScoreLabel,
  Bar,
  FlexRow,
  Block,
  Title,
  TitleWrapper
} from './style';

const ScoreBar = ({ title, scores }) => {
  return (
    <Container>
      <ScoresWrapper>
        <FlexRow>
          {scores.map((s, index) => {
            const { color, active, label, value } = s;
            return (
              <Score key={index}>
                <Block style={{ marginBottom: '2.4rem' }}>
                  <ScoreDash />
                  <ScoreLabel active={active} color={color}>
                    {label}
                  </ScoreLabel>
                  <ScoreDash />
                </Block>
                <Block>
                  <Bar color={color} />
                </Block>
              </Score>
            );
          })}
        </FlexRow>
        <TitleWrapper>
          <Title>{title}</Title>
        </TitleWrapper>
      </ScoresWrapper>
    </Container>
  );
};

ScoreBar.defaultProps = {
  title: 'Valid for 12 months once you fully complete your wealth portfolio. ',
  scores: [
    {
      label: 'c0',
      value: 0,
      color: '#00818D',
      active: false,
      action: () => {}
    },
    {
      label: 'c1',
      value: 1,
      color: '#00BCD4',
      active: false,
      action: () => {}
    },
    {
      label: 'c2',
      value: 2,
      color: '#B2EBF2',
      active: false,
      action: () => {}
    },
    {
      label: 'c3',
      value: 3,
      color: '#C7D3FF',
      active: true,
      action: () => {}
    },
    {
      label: 'c4',
      value: 4,
      color: '#2742C4',
      active: false,
      action: () => {}
    },
    {
      label: 'c5',
      value: 5,
      color: '#011371',
      active: false,
      action: () => {}
    }
  ]
};

ScoreBar.propTypes = {
  title: PropTypes.string,
  scores: PropTypes.arrayOf(PropTypes.shape())
};

export default ScoreBar;
