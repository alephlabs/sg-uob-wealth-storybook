import React, { useEffect, useState, useCallback } from 'react';
import PropTypes from 'prop-types';
import { Container, Inner, Step, Arrow } from './style';
/*
    generate progress steps based on steps props.
*/
const Progress = ({ steps }) => {
  return (
    <Container>
      <Inner>
        {steps.map((s, index) => {
          const { label, active } = s;
          return (
            <Step active={active} key={index}>
              {label}
              {index < steps.length - 1 && <Arrow />}
            </Step>
          );
        })}
      </Inner>
    </Container>
  );
};

Progress.defaultProps = {
  steps: [
    {
      label: 'Risk Level',
      key: 'risk',
      active: true,
      action: () => {}
    },
    {
      label: 'Max Threshold',
      key: 'threshold',
      active: false,
      action: () => {}
    },
    {
      label: 'Personality and Behaviour',
      key: 'behaviour',
      active: false,
      action: () => {}
    }
  ]
};

Progress.propTypes = {
  steps: PropTypes.arrayOf(PropTypes.shape())
};

export default Progress;
