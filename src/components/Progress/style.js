import styled from 'styled-components';
import ArrowBg from 'assets/images/components/Progress/icon-arrow-right.png';

export const Container = styled.div`
  overflow: visible;
  color: #fff;
  margin: 5rem;
`;
export const Inner = styled.div`
  display: flex;
  flex-direction: row;
  overflow: visible;
`;

export const Step = styled.div`
  position: relative;
  font-size: 1.6rem;
  border-top: 0.2rem solid #fff;
  opacity: ${({ active }) => (active ? '1' : '0.2')};
  max-width: 28.4rem;
  padding: 1.2rem 8.6rem 1.2rem 0;
  overflow: visible;
`;

export const Arrow = styled.i`
  display: block;
  position: absolute;
  width: 1.2rem;
  height: 1.9rem;
  background: url(${ArrowBg}) top center no-repeat;
  background-size: cover;
  top: -1.1rem;
  right: -0.6rem;
  z-index: 10;
`;
