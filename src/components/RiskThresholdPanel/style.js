import styled from 'styled-components';
import { theme } from 'global/index';

export const Container = styled.div`
  overflow: visible;
  color: #fff;
  margin-bottom: 9rem;
`;

export const Title = styled.div`
  margin-bottom: 3.2rem;
`;

export const Inner = styled.div`
  min-height: 20rem;
  background: #fff;
  border-radius: 0.6rem;
  padding: 4rem 6.8rem 4rem 4rem;
  color: ${theme.brand};
`;

export const Step = styled.div`
  font-size: 1.6rem;
  border-top: 0.2rem solid #fff;
  opacity: ${({ active }) => (active ? '1' : '0.2')};
  max-width: 28.4rem;
  padding: 1.2rem 8.6rem 1.2rem 0;
  overflow: visible;
`;

export const GraphWrapper = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: flex-start;
`;

export const Graph = styled.div`
  width: 100%;
  color: ${theme.greyA2};
  font-size: 1.2rem;
`;

export const Desc = styled.div`
  margin-left: 4.5rem;
  max-width: 25.2rem;
`;

export const DescSubtitle = styled.div`
  color: ${theme.grey600};
  font-size: 1.2rem;
  margin-bottom: 1.6rem;
`;
export const DescResult = styled.div`
  color: ${theme.primary};
  font-size: 2.8rem;
  margin-bottom: 0.4rem;
`;
export const DescDetail = styled.div`
  color: ${theme.grey600};
  font-size: 1.6rem;
`;

export const UpperQuad = styled.div`
  position: relative;
  height: ${({ height }) => height}rem;
  border-bottom: 0.1rem dashed ${theme.grey300};
`;

export const MiddleAxis = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: flex-start;
  width: 100%;
  padding-left: 13.5rem;
  position: absolute;
  bottom: -2.4rem;
  height: 4.8rem;
`;

export const MiddleAxisDot = styled.div`
  position: relative;
  margin-right: 1.6rem;
  height: 4.8rem;
  width: 4.8rem;
  border-radius: 50%;
  background: #fff;
  cursor: pointer;
`;

export const DotCover = styled.div`
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  z-index: 100;
  background: #fff;
  border-radius: 50%;
  border: 0.1rem dashed ${theme.grey300};
`;

export const UpperBar = styled.div`
  position: absolute;
  left: 13.54%;
  height: ${({ height }) => height}rem;
  width: 71.74%;
  bottom: 2.4rem;
  background: ${theme.cyan200};
  border-top-left-radius: 0.6rem;
  border-top-right-radius: 0.6rem;
  cursor: pointer;
  opacity: ${({ active }) => (active ? 1 : 0.2)};
  transition: opacity ease 0.3s;
  z-index: 90;
`;

export const UpperBarLabel = styled.div`
  position: absolute;
  left: -1.35rem;
  top: -4rem;
  font-size: 1.4rem;
  width: 6rem;
  text-align: center;
`;

export const LowerBar = styled.div`
  position: absolute;
  left: 13.54%;
  height: ${({ height }) => height}rem;
  width: 71.74%;
  top: 2.4rem;
  background: ${theme.marine200};
  border-bottom-left-radius: 0.6rem;
  border-bottom-right-radius: 0.6rem;
  cursor: pointer;
  opacity: ${({ active }) => (active ? 1 : 0.2)};
  transition: opacity ease 0.3s;
  z-index: 90;
`;

export const LowerBarLabel = styled.div`
  position: absolute;
  left: -1.35rem;
  bottom: -4rem;
  font-size: 1.4rem;
  width: 6rem;
  text-align: center;
`;

export const UpperLabel = styled.div`
  position: absolute;
  left: 0;
  bottom: 1.6rem;
  max-width: 10.3rem;
`;

export const LowerQuad = styled.div`
  position: relative;
  height: ${({ height }) => height}rem;
`;

export const LowerLabel = styled.div`
  position: absolute;
  left: 0;
  top: 0.8rem;
  max-width: 8.2rem;
`;
