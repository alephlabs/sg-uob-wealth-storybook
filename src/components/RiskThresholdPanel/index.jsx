import React, { useEffect, useState, useCallback } from 'react';
import PropTypes from 'prop-types';
import max from 'lodash/max';
import {
  Container,
  Inner,
  Title,
  Step,
  GraphWrapper,
  Graph,
  Desc,
  UpperQuad,
  LowerQuad,
  UpperLabel,
  LowerLabel,
  MiddleAxis,
  MiddleAxisDot,
  DotCover,
  UpperBar,
  LowerBar,
  UpperBarLabel,
  LowerBarLabel,
  DescSubtitle,
  DescResult,
  DescDetail
} from './style';

/*
    risk threshold panel selection
*/
const thresholdMap = {
  0: 1.65,
  1: 3.3,
  3: 4.4,
  4: 5,
  8: 7.2,
  13: 10.3,
  19: 13.7,
  24: 16.6,
  27: 18.4,
  32: 21.3
};

const scoreMap = {
  1: -1,
  2: 45,
  3: 7,
  4: 13,
  5: 42,
  6: 44
};

const RiskThresholdPanel = ({
  title,
  upperLabel,
  lowerLabel,
  thresholds,
  onSelect
}) => {
  const [innerThresholds, setInnerThresholds] = useState(
    thresholds.map(t => ({ ...t, score: scoreMap[t.order] }))
  );
  // extra 4 rem is the top and bottom margin
  const upperQuadHeight = thresholdMap[max(thresholds.map(t => t.upper))] + 4;
  const lowerQuadHeight = thresholdMap[max(thresholds.map(t => t.lower))] + 4;
  const [activeThreshold] = innerThresholds.filter(it => it.active);
  const handleSelectThreshold = order => {
    setInnerThresholds(
      innerThresholds.map((it, i) => {
        if (it.order === order) {
          onSelect(i, it);
          return { ...it, active: true };
        }
        return { ...it, active: false };
      })
    );
  };

  return (
    <Container>
      <Inner>
        <Title>{title}</Title>
        <GraphWrapper>
          <Graph>
            <UpperQuad height={upperQuadHeight}>
              <UpperLabel>{upperLabel}</UpperLabel>
              <MiddleAxis>
                {innerThresholds.map((t, index) => {
                  const { upper, active, lower, order } = t;
                  return (
                    <MiddleAxisDot
                      key={index}
                      onClick={() => handleSelectThreshold(order)}
                    >
                      <DotCover />
                      <UpperBar height={thresholdMap[upper]} active={active}>
                        <UpperBarLabel>{`+${upper}%`}</UpperBarLabel>
                      </UpperBar>
                      <LowerBar height={thresholdMap[lower]} active={active}>
                        <LowerBarLabel>{`-${lower}%`}</LowerBarLabel>
                      </LowerBar>
                    </MiddleAxisDot>
                  );
                })}
              </MiddleAxis>
            </UpperQuad>
            <LowerQuad height={lowerQuadHeight}>
              <LowerLabel>{lowerLabel}</LowerLabel>
            </LowerQuad>
          </Graph>
          {activeThreshold && (
            <Desc>
              <DescSubtitle>Your risk LEVEL</DescSubtitle>
              <DescResult>{activeThreshold.label}</DescResult>
              <DescDetail>{activeThreshold.desc}</DescDetail>
            </Desc>
          )}
        </GraphWrapper>
      </Inner>
    </Container>
  );
};

RiskThresholdPanel.defaultProps = {
  title: 'Select an option that represents your risk threshold.',
  upperLabel: 'Potential returns (% per year)',
  lowerLabel: 'Potential loss (% per year)',
  thresholds: [
    {
      score: -1,
      upper: 1,
      active: false,
      lower: 0,
      label: 'Very Low',
      desc:
        'You prefer to maximise your returns and is comfortable with the possibility of losing more than your initial investments.'
    },
    {
      score: 45,
      upper: 4,
      active: false,
      lower: 1,
      label: 'Low',
      desc:
        'You prefer to maximise your returns and is comfortable with the possibility of losing more than your initial investments.'
    },
    {
      score: 7,
      upper: 8,
      active: false,
      lower: 3,
      label: 'Moderate',
      desc:
        'You prefer to maximise your returns and is comfortable with the possibility of losing more than your initial investments.'
    },
    {
      score: 13,
      upper: 13,
      active: false,
      lower: 8,
      label: 'Moderate',
      desc:
        'You prefer to maximise your returns and is comfortable with the possibility of losing more than your initial investments.'
    },
    {
      score: 42,
      upper: 24,
      active: false,
      lower: 19,
      label: 'High',
      desc:
        'You prefer to maximise your returns and is comfortable with the possibility of losing more than your initial investments.'
    },
    {
      score: 44,
      upper: 32,
      active: false,
      lower: 27,
      label: 'Very High',
      desc:
        'You prefer to maximise your returns and is comfortable with the possibility of losing more than your initial investments.'
    }
  ],
  onSelect: (index, threshold) => {}
};

RiskThresholdPanel.propTypes = {
  title: PropTypes.string,
  upperLabel: PropTypes.string,
  lowerLabel: PropTypes.string,
  thresholds: PropTypes.arrayOf(PropTypes.shape()),
  onSelect: PropTypes.func
};

export default RiskThresholdPanel;
