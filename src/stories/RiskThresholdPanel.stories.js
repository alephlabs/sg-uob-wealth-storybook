import React from 'react';
import { action } from '@storybook/addon-actions';
import RiskThresholdPanel from 'components/RiskThresholdPanel';
import 'global/global.css';

export default {
  title: 'RiskThresholdPanel',
  component: RiskThresholdPanel,
  parameters: {
    backgrounds: [
      { name: 'twitter', value: '#00aced' },
      { name: 'facebook', value: '#3b5998', default: true }
    ]
  }
};

const defaultProps = {
  title: 'Select an option that represents your risk threshold.',
  upperLabel: 'Potential returns (% per year)',
  lowerLabel: 'Potential loss (% per year)',
  thresholds: [
    {
      score: -1,
      upper: 1,
      active: false,
      lower: 0,
      label: 'Very Low',
      desc:
        'You prefer to maximise your returns and is comfortable with the possibility of losing more than your initial investments.'
    },
    {
      score: 45,
      upper: 4,
      active: false,
      lower: 1,
      label: 'Low',
      desc:
        'You prefer to maximise your returns and is comfortable with the possibility of losing more than your initial investments.'
    },
    {
      score: 7,
      upper: 8,
      active: false,
      lower: 3,
      label: 'Moderate',
      desc:
        'You prefer to maximise your returns and is comfortable with the possibility of losing more than your initial investments.'
    },
    {
      score: 13,
      upper: 13,
      active: false,
      lower: 8,
      label: 'Moderate',
      desc:
        'You prefer to maximise your returns and is comfortable with the possibility of losing more than your initial investments.'
    },
    {
      score: 42,
      upper: 24,
      active: false,
      lower: 19,
      label: 'High',
      desc:
        'You prefer to maximise your returns and is comfortable with the possibility of losing more than your initial investments.'
    },
    {
      score: 44,
      upper: 32,
      active: false,
      lower: 27,
      label: 'Very High',
      desc:
        'You prefer to maximise your returns and is comfortable with the possibility of losing more than your initial investments.'
    }
  ],
  onSelect: (index, threshold) => {}
};

export const Default = () => <RiskThresholdPanel {...defaultProps} />;
