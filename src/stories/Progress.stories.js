import React from 'react';
import { action } from '@storybook/addon-actions';
import Progress from 'components/Progress';
import 'global/global.css';

export default {
  title: 'Progress',
  component: Progress,
  parameters: {
    backgrounds: [
      { name: 'twitter', value: '#00aced' },
      { name: 'facebook', value: '#3b5998', default: true }
    ]
  }
};

const defaultSteps = [
  {
    label: 'Risk Level',
    key: 'risk',
    active: true,
    action: () => {}
  },
  {
    label: 'Max Threshold',
    key: 'threshold',
    active: false,
    action: () => {}
  },
  {
    label: 'Personality and Behaviour',
    key: 'behaviour',
    active: false,
    action: () => {}
  }
];

export const Default = () => <Progress steps={defaultSteps} />;
