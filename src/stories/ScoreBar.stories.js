import React from 'react';
import { action } from '@storybook/addon-actions';
import ScoreBar from 'components/ScoreBar';
import 'global/global.css';

export default {
  title: 'ScoreBar',
  component: ScoreBar,
  parameters: {
    backgrounds: [
      { name: 'twitter', value: '#00aced' },
      { name: 'facebook', value: '#3b5998', default: true }
    ]
  }
};

const defaultProps = {
  title: 'Valid for 12 months once you fully complete your wealth portfolio. ',
  scores: [
    {
      label: 'c0',
      value: 0,
      color: '#00818D',
      active: false,
      action: () => {}
    },
    {
      label: 'c1',
      value: 1,
      color: '#00BCD4',
      active: false,
      action: () => {}
    },
    {
      label: 'c2',
      value: 2,
      color: '#B2EBF2',
      active: false,
      action: () => {}
    },
    {
      label: 'c3',
      value: 3,
      color: '#C7D3FF',
      active: true,
      action: () => {}
    },
    {
      label: 'c4',
      value: 4,
      color: '#2742C4',
      active: false,
      action: () => {}
    },
    {
      label: 'c5',
      value: 5,
      color: '#011371',
      active: false,
      action: () => {}
    }
  ]
};

export const Default = () => <ScoreBar {...defaultProps} />;
